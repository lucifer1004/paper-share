require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test 'should get root' do
    get root_url
    assert_response :success
    assert_select "title", "Home | SciEntrance"
  end

  test 'should get help' do
    get help_url
    assert_response :success
    assert_select "title", "Help | SciEntrance"
  end

  test 'should get about' do
    get about_url
    assert_response :success
    assert_select "title", "About | SciEntrance"
  end
end
