# README

- 注意在开发环境下不会真的发送邮件，需要在服务器的日志中寻找激活链接。
- 分页工具使用'kaminari', 'kaminari-mongoid'（后者为前者的mongoid接口）
- 目前还没有使用React。后面可能要处理Asset Pipeline和Webpack共存的问题。