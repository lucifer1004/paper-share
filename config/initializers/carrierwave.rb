require 'fog/aws'

# if Rails.env.development?

#  CarrierWave.configure do |config|
#    # config.fog_provider = 'fog/aws'
#    config.fog_credentials = {
#        # Amazon S3 的配置
#        :provider => 'AWS',
#        :aws_access_key_id => 'AKIAJW46UNFMCJA2775Q',
#        :aws_secret_access_key => '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',
#        :region => 'us-west-1'
#    }
#    config.fog_directory = 'railstutorial-wzh'
#  end
# end

if Rails.env.production?
  CarrierWave.configure do |config|
    # config.fog_provider = 'fog/aws'
    config.fog_credentials = {
    # Amazon S3 的配置
      :provider => 'AWS',
      :aws_access_key_id => ENV['S3_ACCESS_KEY'],
      :aws_secret_access_key => ENV['S3_SECRET_KEY'],
      :region => 'us-west-1'
    }
    config.fog_directory = ENV['S3_BUCKET']
  end
end

CarrierWave::SanitizedFile.sanitize_regexp = /[^[:word:]\.\-\+]/