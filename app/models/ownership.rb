class Ownership
  include Mongoid::Document
  include Mongoid::Timestamps
  field :state, type: Integer, default: 0

  belongs_to :owner,     class_name: 'User',
                         inverse_of: :active_ownerships
  belongs_to :belonging, class_name: 'Article',
                         inverse_of: :passive_ownerships

end
