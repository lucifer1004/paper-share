class Activity
  include Mongoid::Document
  include Mongoid::Timestamps
  field :user_id, type: String
  field :article_id, type: String
  field :following_id, type: String
  field :state, type: Integer, default: 0
  # 0 = add article, 1 = comment article, 2 = follow new person
  belongs_to :user

  validates :user_id, presence: true
end
