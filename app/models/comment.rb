class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  field :context, type: String
  field :reply_to, type: String
  belongs_to :article
  belongs_to :user, counter_cache: true

  default_scope -> { order(created_at: :desc) }

  validates :article_id, presence: true
  validates :user_id, presence: true
end
