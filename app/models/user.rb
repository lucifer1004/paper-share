class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include BCrypt

  field :name, type: String
  field :email, type: String
  field :password_digest, type: String
  field :remember_digest, type: String
  field :activation_digest, type: String
  field :reset_digest, type: String
  field :admin, type: Boolean, default: false
  field :activated, type: Boolean, default: false
  field :activated_at, type: Time
  field :reset_sent_at, type: Time

  # has_and_belongs_to_many :owner_of, inverse_of: :owner, class_name: 'Article'
  has_many :active_relationships,  class_name:  'Relationship',
                                   foreign_key: 'follower_id',
                                   dependent:   :destroy
  has_many :passive_relationships, class_name:  'Relationship',
                                   foreign_key: 'followed_id',
                                   dependent:   :destroy
  has_many :active_ownerships,     class_name: 'Ownership',
                                   foreign_key: 'owner_id',
                                   dependent: :destroy
  has_many :comments
  has_many :activities


  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  validates :name,  presence: true, length: { minimum: 5, maximum: 50 }
  validates :email, presence: true, length: { minimum: 5, maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: true
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  begin
    require 'bcrypt'
  rescue LoadError
    $stderr.puts "You don't have bcrypt installed in your application. Please add it to your Gemfile and run bundle install"
    raise
  end

  validate do |record|
    record.errors.add(:password, :blank) unless record.password_digest.present?
  end

  validates_length_of :password, maximum: ActiveModel::SecurePassword::MAX_PASSWORD_LENGTH_ALLOWED
  validates_confirmation_of :password, if: -> { password.present? }

  attr_reader   :password
  attr_accessor :remember_token, :activation_token, :reset_token
  before_save   :downcase_email
  before_create :create_activation_digest

  def authenticate(unencrypted_password)
    BCrypt::Password.new(password_digest) == unencrypted_password && self
  end

  def password=(unencrypted_password)
    if unencrypted_password.nil?
      self.password_digest = nil
    elsif unencrypted_password.present?
      @password = unencrypted_password
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
      self.password_digest = BCrypt::Password.create(unencrypted_password, cost: cost)
    end
  end

  attr_writer :password_confirmation

  # 返回指定字符串的哈希摘要
  def self.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
               BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # 返回一个随机令牌
  def self.new_token
    SecureRandom.urlsafe_base64
  end

  # 为了持久保存会话,在数据库中记住用户
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # 如果指定的令牌和摘要匹配,返回 true
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # 忘记用户
  def forget
    update_attribute(:remember_digest, nil)
  end

  # 激活账户
  def activate
    update_attributes(activated: true, activated_at: Time.zone.now)
  end

  # 发送激活邮件
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  # 设置密码重设相关的属性
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # 发送密码重设邮件
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # 如果密码重设请求超时了，返回 true
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  def following
    self.active_relationships.map(&:followed)
  end

  def following_ids
    self.following.map(&:id)
  end

  def followers
    self.passive_relationships.map(&:follower)
  end

  def articles
    self.active_ownerships.desc(:created_at).map(&:belonging)
  end

  # 关注另一个用户
  def follow(other_user)
    self.active_relationships.create(followed: other_user)
    self.activities.create(following_id: other_user.id, state: 2)
  end

  # 取消关注另一个用户
  def unfollow(other_user)
    self.active_relationships.find_by(followed: other_user).destroy
    self.activities.find_by(following_id: other_user.id).destroy unless self.activities.find_by(following_id: other_user.id).nil?
  end

  def following?(user)
    !self.active_relationships.find_by(followed_id: user.id).nil?
  end

  # 实现动态流原型
  def feed
    Activity.in(user_id: following_ids + [id]).desc(:created_at)
  end

  private

  # 把电子邮件地址转换成小写
  def downcase_email
    self.email = email.downcase
  end

  # 创建并赋值激活令牌和摘要
  def create_activation_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest(activation_token)
  end
end
