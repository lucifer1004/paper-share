class Relationship
  include Mongoid::Document
  include Mongoid::Timestamps
  field :follower_id, type: String
  field :followed_id, type: String

  # inverse_of is necessary, otherwise it will be ambiguous.
  belongs_to :follower, class_name: "User",
                        inverse_of: :active_relationships
  belongs_to :followed, class_name: "User",
                        inverse_of: :passive_relationships

  validates :follower_id, presence: true
  validates :followed_id, presence: true

  index({ follower_id: 1, followed_id: 1 }, { unique: true })
end
