# the Article class
class Article
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Attributes::Dynamic
  field :title, type: String
  field :subject, type: String
  field :published_online, type: Date
  field :doi, type: String
  field :pdf, type: String
  field :pdf_digest, type: String
  field :original_filename, type: String
  validates :pdf, presence: true

  # has_and_belongs_to_many :owner, inverse_of: :owner_of, class_name: 'User'
  has_many :comments
  has_many :passive_ownerships, class_name: 'Ownership',
                                foreign_key: 'belonging_id',
                                dependent: :destroy
  has_many :recommendations
  mount_uploader :pdf, PdfUploader

  def owners
    passive_ownerships.map(&:owner)
  end
end
