class Recommendation
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :article

  field :recommended_id, type: String
  field :intensity, type: Float, default: 0.0
end
