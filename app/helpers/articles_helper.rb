module ArticlesHelper
  require 'em-http-request'
  require 'json'
  require 'pdf-reader'

  def doi_crossref(doi = '')
    if doi.nil?
    else
      EventMachine.run do
        crossref = 'https://api.crossref.org/works/'
        http = EventMachine::HttpRequest.new(crossref + doi.to_s).get
        http.errback do
          EventMachine.stop
        end
        http.callback do
          j = JSON.parse(http.response)
          EventMachine.stop
          return j
        end
      end
    end
  end

  def doi_match(url = '')
    regex = %r{10(\.[0-9]+)+(\/([\-:()\w]+)(\.[\-:()\w]+)*)+}
    reader = PDF::Reader.new(url)
    str = ''

    (0..reader.page_count - 1).each do |i|
      begin
        str += reader.pages[i].to_s
      rescue StandardError
        break
      end
    end

    doi = nil
    doi = regex.match(str).to_s if regex.match?(str)
  end

end