module ApplicationHelper
  class CustomRender < Redcarpet::Render::HTML
    def block_code(code, lang)
      if lang == 'latex'
        return code
      else
        return %(<code class="#{lang}"><pre>#{code}</pre></code>)
      end
    end

    def codespan(code)
      if code.start_with?("$$") and code.end_with?("$$")
        return code
      else
        return %(<code>#{code}</code>)
      end
    end
  end

  # 根据所在的页面返回完整的标题
  def full_title(page_title = '')
    base_title = "SciEntrance"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def markdown(content)
    renderer = CustomRender.new()
    originalrenderer = Redcarpet::Render::HTML.new(hard_wrap: true, filter_html: true)
    options = {
        autolink: true,
        no_intra_emphasis: true,
        disable_indented_code_blocks: true,
        fenced_code_blocks: true,
        lax_html_blocks: true,
        strikethrough: true,
        superscript: true
    }
    Redcarpet::Markdown.new(renderer).render(content).html_safe
  end
end
