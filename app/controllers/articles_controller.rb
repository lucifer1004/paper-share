class ArticlesController < ApplicationController
  before_action :logged_in_user, only: %i[create destroy]
  before_action :correct_user, only: [:show, :destroy]

  def create
    @user = current_user
    @article = Article.new(article_params)
    unless @article.pdf.url.nil?
      tmpdir = File.expand_path("..", @article.pdf.url)
      apath = Rails.root.to_s + '/public' + tmpdir + '/' + @article.original_filename
      @article.pdf_digest = Digest::MD5.hexdigest(File.open(apath, 'r'){|fs| fs.read})
      @article_sim = Article.find_by(pdf_digest: @article.pdf_digest)
      if @article_sim.nil?
        if @article.doi = doi_match(apath)
          doi_message = doi_crossref(@article.doi)['message']
          unless doi_message['published-online'].nil?
            published_time = doi_message['published-online']['date-parts'][0]
            @article.published_online = Date.new(published_time[0],published_time[1],published_time[2])
          end
          @article.title = doi_message['title'][0].to_s unless doi_message['title'].nil?
          @article.subject = doi_message['subject'][0].to_s unless doi_message['subject'].nil?
        end
        if @article.save
          @article.passive_ownerships.create(owner: @user)
          @user.activities.create(article_id: @article.id)
          flash[:success] = 'Article uploaded!'
          redirect_to library_path
        else
          @feed_items = []
          render 'static_pages/library'
        end
      else
        @article = @article_sim
        if @article.owners.index(current_user).nil?
          @article.passive_ownerships.create(owner: @user)
          @user.activities.create(article_id: @article.id)
          flash[:success] = 'Article uploaded!'
          redirect_to library_path
        else
          flash[:warning] = 'Article already exists!'
          redirect_to library_path
        end
      end
    end
  end

  def show
    store_article(@article)
    @comments = @article.comments.desc(:created_at).page params[:page]
    @comment = @article.comments.build
  end

  def destroy
    @user = current_user
    @article.passive_ownerships.find_by(belonging: @article).destroy
    Activity.find_by(article_id: @article.id, user_id: @user.id).destroy
    flash[:success] = 'Article deleted'
    redirect_back(fallback_location: root_url)
  end

  private

  def article_params
    params.require(:article).permit(:pdf, :original_filename)
  end

  def correct_user
    @article = Article.find_by(id: params[:id])
    redirect_to root_url if @article.nil?
  end
end
