class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @user = current_user
      @article = Article.new
      @feed_items = @user.feed.page(params[:page])
    end
  end

  def help
  end

  def about
  end

  def contact
  end

  def library
    if logged_in?
      @user = current_user
      @article = Article.new
      @feed_items = Kaminari.paginate_array(@user.articles).page(params[:page])
    end
  end
end
