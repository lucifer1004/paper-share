class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :update]

  def create
    @article = current_article
    @comment = @article.comments.build(comment_params)
    @comment.user = current_user
    @user = current_user
    if @comment.save
      @activity = @user.activities.find_by(article_id: @article.id, state: 1)
      if @activity.nil?
        @user.activities.create(article_id: @article.id, state: 1)
      else
        @activity.update_attribute(:updated_at, Time.zone.now)
      end
      flash[:success] = 'Comment uploaded!'
      redirect_to @article
    else
      flash[:warning] = 'Comment failed!'
      redirect_to @article
    end
  end

  def destroy
    @article = current_article
    @comment = @article.comments.find_by(id: params[:id])
    @user = @comment.user
    @activity = @user.activities.find_by(article_id: @article.id, state: 1)
    @comment.destroy
    if @article.comments.find_by(user: @user).nil?
      @activity.destroy
    else
      last_update = @article.comments.find_by(user: @user).updated_at
      @activity.update_attribute(:updated_at, last_update)
    end

    flash[:success] = "Comment deleted"
    redirect_to @article
  end

  def update
    @article = current_article
    @comment = @article.comments.find_by(id: params[:id])
    if @comment.update_attributes!(comment_params)
      flash[:success] = "Comment updated"
      redirect_to @article
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:context, :reply_to)
  end
end